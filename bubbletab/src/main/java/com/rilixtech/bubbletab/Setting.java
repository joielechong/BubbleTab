package com.rilixtech.bubbletab;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.util.AttributeSet;
import java.util.ArrayList;
import java.util.List;

class Setting {
  @ColorInt private int selectedColor;
  @ColorInt private int unselectedColor;
  @ColorInt private int circleColor;
  private float circleRatio;
  private boolean firstIconDifferent;
  private List<Integer> images;
  @DrawableRes private int image0Colored;

  public Setting(Context context, AttributeSet attrs) {
    images = new ArrayList<>();
    final TypedArray arr = context.obtainStyledAttributes(attrs, R.styleable.BubbleTab);
    if (arr == null) return;

    selectedColor = arr.getColor(R.styleable.BubbleTab_btb_selectedColor, Color.WHITE);
    int defaultColorValue = Color.parseColor("#c0c0c0");
    unselectedColor = arr.getColor(R.styleable.BubbleTab_btb_unselectedColor, defaultColorValue);
    circleColor = arr.getInt(R.styleable.BubbleTab_btb_circleColor, Color.BLACK);
    circleRatio = arr.getFloat(R.styleable.BubbleTab_btb_circleRatio, 1.2f);
    image0Colored = arr.getResourceId(R.styleable.BubbleTab_btb_image0Colored, 0);
    add(arr, R.styleable.BubbleTab_btb_image0);
    add(arr, R.styleable.BubbleTab_btb_image1);
    add(arr, R.styleable.BubbleTab_btb_image2);
    add(arr, R.styleable.BubbleTab_btb_image3);
    add(arr, R.styleable.BubbleTab_btb_image4);
    add(arr, R.styleable.BubbleTab_btb_image5);
    add(arr, R.styleable.BubbleTab_btb_image6);
    add(arr, R.styleable.BubbleTab_btb_image7);
    add(arr, R.styleable.BubbleTab_btb_image8);
    add(arr, R.styleable.BubbleTab_btb_image9);
    add(arr, R.styleable.BubbleTab_btb_image10);
    arr.recycle();

    firstIconDifferent = image0Colored != 0;
  }

  private void add(TypedArray array, int res) {
    int value = array.getResourceId(res, 0);
    if (value != 0) images.add(value);
  }

  int getCircleColor() {
    return circleColor;
  }

  float getCircleRatio() {
    return circleRatio;
  }
}
