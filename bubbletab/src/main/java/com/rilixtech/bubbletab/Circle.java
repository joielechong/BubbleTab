package com.rilixtech.bubbletab;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v4.view.ViewPager;
import android.view.View;

class Circle {
  private float lastOffset = 0f; //current shape-offset in percentage (from 0f to 1f)
  private Paint paint = new Paint();
  private float translationX = 1f;
  private int width;
  private float circleRatio = 1f;
  private float scale = 1f;

  Circle() {
    paint.setColor(Color.BLACK);
    paint.setAntiAlias(true);
    paint.setStrokeWidth(1);
    paint.setStyle(Paint.Style.FILL_AND_STROKE);
  }

  void onDraw(Canvas canvas) {
    canvas.save();
    canvas.translate(translationX, 0);
    final int height = canvas.getHeight();
    canvas.drawCircle(width / 2f, height / 2f, (width / 2f) * circleRatio * scale, paint);
    canvas.restore();
  }

  public void setWidth(int width) {
    this.width = width;
  }

  public int getColor() {
    return paint.getColor();
  }

  public void setColor(int color) {
    paint.setColor(color);
  }

  public void setRatio(float circleRatio) {
    this.circleRatio = circleRatio;
  }

  private int valueForFrame(int valueStart, int valueEnd, float frameStart, float frames,
      float currentFrame) {
    return (int) ((valueEnd - valueStart) / frames * (currentFrame - frameStart) + valueStart);
  }

  public void layout(BubbleTab owner, int position, float positionOffset) {
    int widthStart;
    int widthEnd;
    View c = owner.getChildAt(position);
    if (c == null) return;

    widthStart = c.getWidth();
    //animate to next tabs size and reverse
    View next = owner.getChildAt(position + 1);
    if (next == null) {
      widthEnd = widthStart;
    } else {
      widthEnd = valueForFrame(widthStart, next.getWidth(), 0f, 1f, positionOffset);
    }
    setWidth(widthEnd);

    translationX = c.getX() + widthStart * positionOffset;

    float distanceFromMiddle = Math.abs(positionOffset - 0.5f);
    float min = 0f;
    scale = min + (1 - min) * (distanceFromMiddle + 0.5f);

    lastOffset = positionOffset;
  }

  //prepare shape based on "ViewPager" position
  public void layout(BubbleTab owner, ViewPager viewPager) {
    //get current Page-index
    int position = viewPager != null ? viewPager.getCurrentItem() : 0;
    //convert Page-index to position (position is always less than current Page-index)
    if (position > 1) position -= 1;
    layout(owner, position, this.lastOffset);
  }
}
