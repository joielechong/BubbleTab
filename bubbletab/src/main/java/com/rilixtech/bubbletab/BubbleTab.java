package com.rilixtech.bubbletab;

import android.content.Context;
import android.graphics.Canvas;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import java.util.ArrayList;
import java.util.List;

public class BubbleTab extends LinearLayout {
  private int mNumberOfIcons = 0;
  @Nullable private ViewPager mViewPager;
  private Circle mCircle = new Circle();
  private Setting mSetting;
  private List<View> mIcons;
  private final ChangeListener mPageChangeListener = new ChangeListener(this);

  public BubbleTab(Context context) {
    super(context);
    init(context, null);
  }

  public BubbleTab(Context context, AttributeSet attrs) {
    super(context, attrs);
    init(context, attrs);
  }

  public BubbleTab(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init(context, attrs);
  }

  public void setupWithViewPager(final ViewPager viewPager) {
    if (mViewPager != null) mViewPager.removeOnPageChangeListener(mPageChangeListener);

    mViewPager = viewPager;
    mViewPager.addOnPageChangeListener(mPageChangeListener);

    final int currentItem = viewPager.getCurrentItem();
    for (int i = 0; i < mIcons.size(); i++) {
      mIcons.get(i).setSelected(i == currentItem);
    }
    //no need to update here since we override "onLayout(...)"
    //  but still we want to support change of pager more then ones
    mCircle.layout(this, mViewPager);
    postInvalidate();
  }

  @Override
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();
    if (mViewPager != null) {
      mViewPager.addOnPageChangeListener(mPageChangeListener);
      postInvalidate();
    }
  }

  @Override
  protected void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    if (mViewPager != null) mViewPager.removeOnPageChangeListener(mPageChangeListener);
  }

  @Override
  protected void onFinishInflate() {
    super.onFinishInflate();

    mCircle.setColor(mSetting.getCircleColor());
    mCircle.setRatio(mSetting.getCircleRatio());

    mNumberOfIcons = getChildCount();
    mIcons = new ArrayList<>();
    for (int i = 0; i < mNumberOfIcons; i++) {
      final int index = i;
      final View childAt = getChildAt(index);
      mIcons.add(childAt);
      childAt.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
          if (mViewPager != null) mViewPager.setCurrentItem(index, true);
        }
      });
    }
  }

  private void init(Context context, AttributeSet attrs) {
    setWillNotDraw(false);
    mSetting = new Setting(context, attrs);
  }

  @Override
  protected void onLayout(boolean changed, int l, int t, int r, int b) {
    super.onLayout(changed, l, t, r, b);
    //prepare shape even before any "onPageScrolled(...)" event
    mCircle.layout(this, mViewPager);
  }

  @Override
  protected void onDraw(Canvas canvas) {
    mCircle.onDraw(canvas);
    super.onDraw(canvas);
  }

  Circle getCircle() {
    return mCircle;
  }

  List<View> getIcons() {
    return mIcons;
  }

  public int getNumberOfIcons() {
    return mNumberOfIcons;
  }
}
