package com.rilixtech.bubbletab;

import android.support.v4.view.ViewPager;

final class ChangeListener implements ViewPager.OnPageChangeListener {
  private BubbleTab bubbleTab;
  private float oldPositionOffset;
  private boolean toRight;

  ChangeListener(BubbleTab bubbleTab) {
    this.bubbleTab = bubbleTab;
  }

  @Override
  public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    //Log.d("onPageScrolled", position + " : " + positionOffset);

    if (oldPositionOffset == 0) {
      toRight = positionOffset > oldPositionOffset;
    }

    bubbleTab.getCircle().layout(bubbleTab, position, positionOffset);

    if (positionOffset != 0) {
      if (toRight) {
        if (positionOffset < 0.5f) {
          bubbleTab.getIcons().get(position).setSelected(true);
          if (position + 1 < bubbleTab.getNumberOfIcons()) {
            bubbleTab.getIcons().get(position + 1).setSelected(false);
          }
        } else {
          bubbleTab.getIcons().get(position).setSelected(false);
          if (position + 1 < bubbleTab.getNumberOfIcons()) {
            bubbleTab.getIcons().get(position + 1).setSelected(true);
          }
        }
      } else {
        if (positionOffset < 0.5f) {
          bubbleTab.getIcons().get(position).setSelected(true);
          if (position - 1 > 0) {
            bubbleTab.getIcons().get(position + 1).setSelected(false);
          }
        } else {
          bubbleTab.getIcons().get(position).setSelected(false);
          if (position - 1 > 0) {
            bubbleTab.getIcons().get(position + 1).setSelected(true);
          }
        }
      }
    }

    oldPositionOffset = positionOffset;
    bubbleTab.postInvalidate();
  }

  @Override
  public void onPageSelected(int position) {

  }

  @Override
  public void onPageScrollStateChanged(int state) {

  }
}
