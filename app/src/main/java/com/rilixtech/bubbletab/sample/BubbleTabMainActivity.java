package com.rilixtech.bubbletab.sample;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import com.rilixtech.bubbletab.BubbleTab;

public class BubbleTabMainActivity extends AppCompatActivity {

  BubbleTab bubbleTab;
  ViewPager viewPager;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.bubbletab_activity_main);
    bubbleTab = findViewById(R.id.bubbleTab);
    viewPager = findViewById(R.id.viewPager);

    viewPager.setAdapter(new FakeAdapter(getSupportFragmentManager()));
    bubbleTab.setupWithViewPager(viewPager);
  }
}
