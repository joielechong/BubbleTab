# BubbleTab

Put some bubble in your tabs and give your apps a supa fresh style !


[![screen](media/withScreen_cropped.png)](https://gitlab.com/joielechong/BubbleTab)

# Usage

[![screen](media/video.gif)](https://gitlab.com/joielechong/BubbleTab)

Add a BubbleTab with your icons on the layout.xml

Customisable parameters:
- circleColor
- circleRatio

```xml
<com.rilixtech.bubbletab.BubbleTab
        android:id="@+id/bubbleTab"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:clipToPadding="false"
        android:background="@android:color/white"
        android:elevation="10dp"

        app:btb_circleColor="@color/colorAccent"
        app:btb_circleRatio="1.25"
        >

                <ImageView
                    android:layout_width="match_parent"
                    android:layout_height="match_parent"
                    android:layout_weight="1"
                    android:padding="16dp"
                    android:src="@drawable/bubbletab_ic_hourglass_selector" />

                <ImageView
                    android:layout_width="match_parent"
                    android:layout_height="match_parent"
                    android:layout_weight="1"
                    android:padding="16dp"
                    android:src="@drawable/bubbletab_ic_event_selector" />

                <ImageView
                    android:layout_width="match_parent"
                    android:layout_height="match_parent"
                    android:layout_weight="1"
                    android:padding="16dp"
                    android:src="@drawable/bubbletab_ic_query_selector" />

                <ImageView
                    android:layout_width="match_parent"
                    android:layout_height="match_parent"
                    android:layout_weight="1"
                    android:padding="16dp"
                    android:src="@drawable/bubbletab_ic_search_selector" />

                <ImageView
                    android:layout_width="match_parent"
                    android:layout_height="match_parent"
                    android:layout_weight="1"
                    android:padding="16dp"
                    android:src="@drawable/bubbletab_ic_home_selector" />

</com.rilixtech.bubbletab.BubbleTab>

<android.support.v4.view.ViewPager
    android:id="@+id/viewPager"
    android:layout_width="match_parent"
    android:layout_height="match_parent"/>
```

Then bound it with your viewPager

```java
bubbleTab.setupWithViewPager(viewPager);
```

# Customisation

To display your home icon with a different color / content if selected / unselected

Simply use selectors !

[![screen](media/different_icon.gif)](https://gitlab.com/joielechong/BubbleTab)

```xml
<?xml version="1.0" encoding="utf-8"?>
<selector xmlns:android="http://schemas.android.com/apk/res/android">
    <item android:state_selected="true" android:drawable="@drawable/bubbletab_ic_account_selected"/>
    <item android:drawable="@drawable/bubbletab_ic_account"/>
</selector>
```

# Download

**build.gradle**
```
allprojects {
      repositories {
           maven {
               url 'https://jitpack.io'
           }
      }
}
```


```
dependencies {
        implementation 'com.gitlab.joielechong:bubbletab:1.2.0'
}
```
# Credits

Original Author: Florent Champigny [http://www.florentchampigny.com/](http://www.florentchampigny.com/)


License
--------

    Copyright 2018 Rilix Technology
    Copyright 2016 florent37, Inc.

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
